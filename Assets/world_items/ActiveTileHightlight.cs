﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveTileHightlight : MonoBehaviour {

    public Vector2Int pos;
    Image image;
    // Start is called before the first frame update
    void Start() {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update() {
        if (pos == WorldGen.active_tile) {
            image.color = Color.gray;
        } else {
            image.color = Color.white;
        }

    }
}
