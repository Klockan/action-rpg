﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillCountTracker : MonoBehaviour {
    public Killable player;

    Text text;

    // Start is called before the first frame update
    void Start() {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        text.text = "Kills: " + player.kill_count;
    }
}
