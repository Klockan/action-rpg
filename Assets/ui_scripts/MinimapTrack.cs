﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapTrack : MonoBehaviour {

    public GameObject minimap_icon_type;
    GameObject icon;

    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        if (MiniMap.singleton == null) return;
        if (icon == null) {
            icon = Instantiate(minimap_icon_type, MiniMap.singleton.map_icons.transform);
        }
        icon.transform.localPosition = MiniMap.ToMapCoord(transform.position);
    }

    private void OnDestroy() {
        Destroy(icon);
    }
}
