﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour {

    public static MiniMap singleton;

    public GameObject minimap_object;
    public GameObject room;
    public GameObject map_icons;
    WorldGen world;
    bool init = false;

    public static Vector3 ToMapCoord(Vector3 world_coord) {
        float x = world_coord.x;
        float y = world_coord.z;
        return new Vector3(x / 30 * 15, y / 30 * 15, 10);
    }

    private void Draw() {
        foreach (Vector2Int pos in world.open.Keys) {
            float x = pos.x;
            float y = pos.y;
            var new_room = Instantiate(room, minimap_object.transform);
            new_room.transform.localPosition = new Vector3(x * 15, y * 15);
            new_room.GetComponent<ActiveTileHightlight>().pos = pos;
        }
    }

    // Start is called before the first frame update
    void Start() {
        world = GetComponent<WorldGen>();
        singleton = this;
    }

    // Update is called once per frame
    void Update() {
        if (!init) {
            Draw();
            init = true;
        }
    }
}
