﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiTargeting : MonoBehaviour {

    public GameObject target;
    public Pickup target_pickup;
    Killable body;

    float next_update;
    float update_interval = 0.1f;

    public bool Safe() {
        if (target == null) return false;
        return (transform.position - target.transform.position).magnitude > 30;
    }

    public void UpdateTargeting() {
        if (Time.time < next_update) return;
        next_update = Time.time + update_interval;
        target = Killable.getNearestEnemy(body.faction, transform.position);
        if (body.hp < body.max_hp) {
            target_pickup = Pickup.ClosestUnguardedHealthPickup(transform.position);
            if (target_pickup == null) {
                target_pickup = Pickup.ClosestWeaponPickup(transform.position);
            }
        } else {
            target_pickup = Pickup.ClosestWeaponPickup(transform.position);
        }
    }

    public Vector3 TargetCoord() {
        if (target_pickup != null) return target_pickup.transform.position;
        return target.transform.position;
    }

    public bool HasTarget() {
        return target != null || target_pickup != null;
    }

    // Start is called before the first frame update
    void Start() {
        next_update = Time.time;
        body = GetComponent<Killable>();
    }

    // Update is called once per frame
    void Update() {
        if (target == null) {
            UpdateTargeting();
        }
    }
}
