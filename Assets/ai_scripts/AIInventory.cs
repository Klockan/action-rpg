﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIInventory : MonoBehaviour {

    Shooter gun;
    Inventory inventory;
    Weapon active_weapon;

    // Start is called before the first frame update
    void Start() {
        gun = GetComponent<Shooter>();
        inventory = GetComponent<Inventory>();
    }

    // Update is called once per frame
    void Update() {
        foreach (Weapon p in inventory.weapons) {
            if (active_weapon == null || p.power_level >= active_weapon.power_level) {
                active_weapon = p;
                gun.Equip(active_weapon);
            }
        }
    }
}
