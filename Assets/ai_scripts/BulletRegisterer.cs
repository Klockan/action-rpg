﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletRegisterer : MonoBehaviour {

    public List<GameObject> bullets;

    private void OnTriggerEnter(Collider collision) {
        Bullet o = collision.gameObject.GetComponent<Bullet>();
        if (o == null) return;
        bullets.Add(o.gameObject);
    }

    private void OnTriggerExit(Collider collision) {
        Bullet o = collision.gameObject.GetComponent<Bullet>();
        if (o == null || o.gameObject == null) return;
        bullets.Remove(o.gameObject);
    }
    // Start is called before the first frame update
    void Start() {

    }

    void TrimBullets() {
        int n = bullets.Count;
        int cur = 0;
        for (int i = 0; i < n; ++i) {
            if (bullets[i]) {
                bullets[cur++] = bullets[i];
            }
        }
        bullets.RemoveRange(cur, n - cur);
    }

    // Update is called once per frame
    void Update() {
        TrimBullets();
    }
}
