﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterAI : MonoBehaviour {

    Rigidbody me;
    Killable body;

    Shooter gun;

    public float shoot_distance = 20;

    AiTargeting target_ai;

    public bool leads = true;

    // Start is called before the first frame update
    void Start() {
        body = GetComponent<Killable>();
        me = GetComponent<Rigidbody>();
        gun = GetComponent<Shooter>();
        target_ai = GetComponent<AiTargeting>();
        leads = Random.Range(0, 100f) < 25;
    }

    private void RotationHandler() {

        var target = target_ai.target;
        var shoot_loc = target.transform.position;
        if (leads && gun.beam == null) {
            Vector3 tov = (shoot_loc - transform.position);
            Rigidbody t = target.GetComponent<Rigidbody>();
            float time = gun.TimeToDistance(tov.magnitude);
            Vector3 expected_travel =  t.velocity * time;
            float mag_div = tov.magnitude / expected_travel.magnitude;
            if (mag_div < 2) {
                expected_travel = expected_travel * mag_div / 2;
            }
            shoot_loc += expected_travel;
        }
        gun.LookAt(shoot_loc);
    }

    private void StopRotate() {
        gun.LookAt(new Vector3());
    }

    private void ClickHandler() {
        var target = target_ai.target;
        var shoot_loc = target.transform.position;
        if (leads && gun.beam == null) {
            Vector3 tov = (shoot_loc - transform.position);
            Rigidbody t = target.GetComponent<Rigidbody>();
            float time = gun.TimeToDistance(tov.magnitude);
            Vector3 expected_travel = t.velocity * time;
            float mag_div = tov.magnitude / expected_travel.magnitude;
            if (mag_div < 2) {
                expected_travel = expected_travel * mag_div / 2;
            }
            shoot_loc += expected_travel;
        }
        gun.ShootAt(shoot_loc, new Vector3());
    }

    Vector3 VectorToTarget() {
        return target_ai.target.transform.position - transform.position;
    }

    // Update is called once per frame
    void Update() {
        if (target_ai.target != null) {
            RotationHandler();
            if (VectorToTarget().magnitude < shoot_distance) {
                ClickHandler();
            }
        } else {
            StopRotate();
        }
    }
}
