﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverAI : MonoBehaviour {

    Rigidbody me;
    Killable body;

    public static void SmoothAddVelocity(Vector3 v, Rigidbody me) {
        Vector3 o = me.velocity;
        me.velocity = (o + v) / 2;
    }

    public float engage_distance = 20;

    public float min_dist = 5f;
    public float max_dist = 8f;

    AiTargeting target_ai;
    DodgeAI dodge;

    public float speed = 1.0f;


    Shooter gun;

    private void MoveHandler() {
        Vector3 dir = gun.TargetDir(target_ai.target.transform.position);
        Vector3 vel2d = new Vector3();
        float dist = (transform.position - target_ai.target.transform.position).magnitude;
        if (dist > max_dist) {
            vel2d += dir * speed;
        } else if (dist < min_dist) {
            vel2d -= dir * speed * 2;
        } else {
            vel2d += Quaternion.Euler(0, 90, 0) * dir * speed;
        }
        SmoothAddVelocity(new Vector3(vel2d.x, me.velocity.y, vel2d.z), me);
    }
    void Stop() {
        SmoothAddVelocity(new Vector3(0, me.velocity.y, 0), me);
    }

    Vector3 VectorToTarget() {
        return target_ai.target.transform.position - transform.position;
    }

    // Start is called before the first frame update
    void Start() {
        body = GetComponent<Killable>();
        me = GetComponent<Rigidbody>();
        gun = GetComponent<Shooter>();
        target_ai = GetComponent<AiTargeting>();
        dodge = GetComponent<DodgeAI>();
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (dodge != null && dodge.ShouldDodge()) return;
        if (target_ai.target != null) {
            if (VectorToTarget().magnitude < engage_distance) {
                MoveHandler();
            }
        } else {
            Stop();
        }

    }
}
