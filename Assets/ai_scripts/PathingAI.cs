﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PathingAI : MonoBehaviour {

    DodgeAI dodge;

    AiTargeting target_ai;

    public WorldGen map;

    public MoverAI mover_ai;

    List<Vector2Int> cur_path = new List<Vector2Int>();
    int cur_progress = 0;

    public float speed = 1.0f;

    public Vector2Int cur_tile;
    Rigidbody me;



    List<Vector2Int> UnwindPath(List<Tuple<Vector2Int, int, int>>[] stack, int i, int j) {
        List<Tuple<Vector2Int, int, int>> list = stack[i];
        Vector2Int p = list[j].Item1;
        int pi = list[j].Item2;
        int pj = list[j].Item3;
        var cur_t = stack[i][j];
        List<Vector2Int> path = new List<Vector2Int>();
        path.Add(cur_t.Item1);
        while (pi != 0) {
            cur_t = stack[pi][pj];
            path.Add(cur_t.Item1);
            pi = cur_t.Item2;
            pj = cur_t.Item3;
        }
        cur_t = stack[pi][pj];
        path.Add(cur_t.Item1);
        path.Reverse();
        return path;
    }

    static Vector2Int V2(int x, int y) {
        return new Vector2Int(x, y);
    }

    public List<Vector2Int> BfsTo(Vector2Int from_pos, Vector2Int to_pos) {
        if (from_pos == to_pos) return new List<Vector2Int>();
        Dictionary<Vector2Int, bool> taken = new Dictionary<Vector2Int, bool>();
        List<Tuple<Vector2Int, int, int>>[] stack = new List<Tuple<Vector2Int, int, int>>[200];
        stack[0] = new List<Tuple<Vector2Int, int, int>> { new Tuple<Vector2Int, int, int>(from_pos, -1, -1) };
        for (int i = 1; i < stack.Length; ++i) {
            stack[i] = new List<Tuple<Vector2Int, int, int>>();
        }
        for (int i = 0; i < stack.Length; ++i) {
            List<Tuple<Vector2Int, int, int>> list = stack[i];
            for (int j = 0; j < list.Count; ++j) {
                Vector2Int p = list[j].Item1;
                int pi = list[j].Item2;
                int pj = list[j].Item3;
                if (p == to_pos) {
                    return UnwindPath(stack, i, j);
                }
                if (taken.ContainsKey(p)) continue;
                taken[p] = true;
                var vt = V2(0, 0);
                vt = V2(p.x + 1, p.y);
                if (!taken.ContainsKey(vt)) {
                    if (map.HasPos(vt)) {
                        stack[i + 1].Add(new Tuple<Vector2Int, int, int>(vt, i, j));
                    }
                }
                vt = V2(p.x - 1, p.y);
                if (!taken.ContainsKey(vt)) {
                    if (map.HasPos(vt)) {
                        stack[i + 1].Add(new Tuple<Vector2Int, int, int>(vt, i, j));
                    }
                }
                vt = V2(p.x, p.y + 1);
                if (!taken.ContainsKey(vt)) {
                    if (map.HasPos(vt)) {
                        stack[i + 1].Add(new Tuple<Vector2Int, int, int>(vt, i, j));
                    }
                }
                vt = V2(p.x, p.y - 1);
                if (!taken.ContainsKey(vt)) {
                    if (map.HasPos(vt)) {
                        stack[i + 1].Add(new Tuple<Vector2Int, int, int>(vt, i, j));
                    }
                }
            }
        }
        throw new ArgumentException("Couldn't find path.");
    }

    private void MoveDir(Vector3 dir) {
        dir = dir.normalized;
        Vector3 vel2d = new Vector3();
        vel2d += dir * speed;
        if (target_ai.Safe()) vel2d *= 3;
        MoverAI.SmoothAddVelocity(new Vector3(vel2d.x, me.velocity.y, vel2d.z), me);
    }

    void MakeProgress() {
        Vector3 next_target;
        if (cur_progress == cur_path.Count) {
            next_target = target_ai.TargetCoord();
        } else {
            next_target = WorldGen.ToWorldPos(cur_path[cur_progress]);
        }
        Vector3 cur = WorldGen.ToWorldPos(cur_tile);
        if (cur_progress != cur_path.Count && cur == next_target) {
            target_ai.UpdateTargeting();
            cur_progress++;
            MakeProgress();
            return;
        }
        Vector3 tp = transform.position;
        Vector3 mid_section = (next_target - cur) * 0.45f + cur;
        if ((mid_section - tp).magnitude < 2) {
            target_ai.UpdateTargeting();
            mid_section = (next_target - cur) * 0.55f + cur;
        }

        if (cur_progress == cur_path.Count) {
            mid_section = next_target;
            if (target_ai.target_pickup != null) {
                mid_section = target_ai.TargetCoord();
            }
        }

        Vector3 to_target = mid_section - tp;
        MoveDir(to_target);
        if (next_target.magnitude < 1) {
            target_ai.UpdateTargeting();
            cur_progress = 0;
            cur_path.Clear();
        }
    }

    string PathString() {
        string res = "[";
        foreach (var p in cur_path) {
            res += "(" + p.x + ", " + p.y + "), ";
        }
        res += "]";
        return res;
    }

    void PathToTarget() {
        if (!target_ai.HasTarget()) return;
        var target_coord = WorldGen.ToMapPos(target_ai.TargetCoord());
        if (cur_tile == target_coord) return;
        Debug.Log("Calculated Path");
        cur_path = BfsTo(cur_tile, target_coord);
        Debug.Log(PathString());
        cur_progress = 0;
    }

    void MaybeUpdatePath() {
        if (!target_ai.HasTarget()) return;
        var target_pos = WorldGen.ToMapPos(target_ai.TargetCoord());
        if (cur_path[cur_path.Count - 1] != target_pos) {
            PathToTarget();
        }
    }

    // Start is called before the first frame update
    void Start() {
        me = GetComponent<Rigidbody>();
        mover_ai = GetComponent<MoverAI>();
        target_ai = GetComponent<AiTargeting>();
        dodge = GetComponent<DodgeAI>();
    }

    bool InEnemyTile() {
        if (target_ai.target == null) return false;
        var target_coord = WorldGen.ToMapPos(target_ai.target.transform.position);
        return target_coord == cur_tile;
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (dodge != null && dodge.ShouldDodge()) return;
        if (!target_ai.HasTarget()) return;
        cur_tile = WorldGen.ToMapPos(transform.position);
        WorldGen.active_tile = cur_tile;
        if (InEnemyTile()) {
            mover_ai.engage_distance = 30;
            return;
        } else {
            Pickup.MarkCleared(cur_tile);
            target_ai.UpdateTargeting();
            mover_ai.engage_distance = 1;
        }
        if (cur_path.Count > 0) {
            MaybeUpdatePath();
            MakeProgress();
        } else {
            PathToTarget();
            if (cur_path.Count > 0) {
                mover_ai.engage_distance = 1;
                MaybeUpdatePath();
                MakeProgress();
            }
        }
    }
}
