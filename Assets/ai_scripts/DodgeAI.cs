﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DodgeAI : MonoBehaviour {

    public BulletRegisterer bullets;

    public float speed = 1.0f;

    public int dodge_version = 1;

    Shooter gun;

    Rigidbody me;
    Killable body;

    // Start is called before the first frame update
    void Start() {
        body = GetComponent<Killable>();
        me = GetComponent<Rigidbody>();
        gun = GetComponent<Shooter>();
    }

    public bool ShouldDodge() {
        return bullets.bullets.Count > 0;
    }

    void Dodge1() {
        foreach (GameObject bullet in bullets.bullets) {
            if (bullet == null) continue;
            // Dodging single bullet.
            Bullet raw = bullet.GetComponent<Bullet>();
            if (raw.original_shooter != null && raw.original_shooter.faction == body.faction) continue;
            Vector3 dir = gun.TargetDir(bullet.transform.position);
            Vector3 vel2d = new Vector3();
            if (Vector3.Dot(raw.dir, dir) > 0) {
                // No dodge, behind bullet.
                vel2d -= dir * speed;
            } else {
                Vector3 orthogonal_dir = Quaternion.Euler(0, 90, 0) * dir;
                float dist = (transform.position - bullet.transform.position).magnitude;
                if (Vector3.Dot(raw.dir, orthogonal_dir) > 0) {
                    vel2d -= orthogonal_dir * speed;
                } else {
                    vel2d += orthogonal_dir * speed;
                }
            }
            MoverAI.SmoothAddVelocity(new Vector3(vel2d.x, me.velocity.y, vel2d.z), me);
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (dodge_version == 1) {
            Dodge1();
        }
    }
}
