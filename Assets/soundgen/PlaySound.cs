﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour {

    int frequency = 57000;


    public double magnitude = 0.1f;
    public double falloff_per_sec = 10;

    public int type = 0;

    public float tone_hz = 500;

    static AudioClip.PCMReaderCallback DongGen(float tone_hz, float frequency, double falloff_per_sec, double magnitude) {
        int cur = 0;
        float tone = tone_hz * Mathf.PI * 2 / frequency;
        return data => {
            double falloff = 1 - falloff_per_sec / 57000;
            for (int i = 0; i < data.Length; ++i) {
                float cur_t = cur++;
                data[i] = Mathf.Sin(cur_t * tone) * (float)magnitude;
                magnitude = magnitude * falloff;
            }
        };
    }

    static AudioClip.PCMReaderCallback TinkGen(float tone_hz, float frequency, double falloff_per_sec, double magnitude) {
        int cur = 0;
        float tone = tone_hz * Mathf.PI * 2 / frequency;
        float tone2 = tone * 1.5f;
        float tone3 = tone * 1.3f;
        float tone4 = tone * 1.7f;
        return data => {
            double falloff = 1 - falloff_per_sec / 57000;
            for (int i = 0; i < data.Length; ++i) {
                float cur_t = cur++;
                float val = Mathf.Sin(cur_t * tone) + Mathf.Sin(cur_t * tone2) + Mathf.Sin(cur_t * tone3) + Mathf.Sin(cur_t * tone4);
                val = val / 4;
                data[i] = val * (float)magnitude;
                magnitude = magnitude * falloff;
            }
        };
    }

    public int rando_per_sec = 1000;
    public double rando_falloff = 0.9;
    public float crash_duration = 1;

    static AudioClip.PCMReaderCallback CrashGen(double magnitude, int rando_per_sec, double falloff, float duration) {
        int cur = 0;
        float end_sound = duration * 57000;
        double lead_signal = 0;
        int rando = 1;
        int rando_mul = 853;
        int rando_mod = 997;
        double signal_mag = 1 - falloff;
        int samples_per_rando = 57000 / rando_per_sec;

        int next_rando = samples_per_rando;

        double cur_val = 0;

        return data => {
            for (int i = 0; i < data.Length; ++i) {
                if (next_rando-- <= 0) {
                    next_rando = samples_per_rando;
                    rando = rando * rando_mul % rando_mod;
                    lead_signal = (rando - 500) / 500f;
                }
                float cur_t = cur++;
                cur_val = cur_val * falloff + lead_signal * signal_mag;
                float val = (float)(magnitude * cur_val);
                float end_mag = (end_sound - cur_t) / end_sound;
                if (end_mag < 0) end_mag = 0;
                val = val * end_mag;
                if (val > 1) throw new System.Exception();
                data[i] = val;
            }
        };
    }

    AudioClip MakeAudio(AudioClip.PCMReaderCallback type) {
        int length_sampeles = 100000;
        int channels = 1;
        bool stream = true;
        return AudioClip.Create("MyClip", length_sampeles, channels, frequency, stream, type);
    }

    AudioSource my_source;

    // Start is called before the first frame update
    void Start() {
        my_source = GetComponent<AudioSource>();
        if (type == 0) {
            my_source.clip = MakeAudio(DongGen(tone_hz, frequency, falloff_per_sec, magnitude));
        }
        if (type == 1) {
            my_source.clip = MakeAudio(TinkGen(tone_hz, frequency, falloff_per_sec, magnitude));
        }
        if (type == 2) {
            my_source.clip = MakeAudio(CrashGen(magnitude, rando_per_sec, rando_falloff, crash_duration));
        }
        my_source.Play();
    }

    // Update is called once per frame
    void Update() {
    }
}
