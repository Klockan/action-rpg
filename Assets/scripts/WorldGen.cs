﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WorldGen : MonoBehaviour {

    public static Vector2Int active_tile;

    public GameObject map_segment;
    public GameObject wall_segment;
    public GameObject enemy_group;
    public GameObject health_pickup;
    public List<GameObject> weapon_pickups;

    public int enemy_scaling = 10;

    public float weapon_pickup_rate = 10f;

    public int room_count = 10;

    public static Vector2Int ToMapPos(Vector3 pos) {
        int x = (int)Math.Round(pos.x / 30);
        int y = (int)Math.Round(pos.z / 30);
        return new Vector2Int(x, y);
    }

    public static Vector3 ToWorldPos(Vector2Int map_pos) {
        return new Vector3(map_pos.x * 30, 0, map_pos.y * 30);
    }

    void CreateWallSegment(int x, int y, GameObject tile) {
        float left_begin = -14f;
        float down_begin = -14f;
        GameObject wall = Instantiate(wall_segment, tile.transform);
        wall.transform.localPosition = new Vector3(left_begin + x * 2, 0, down_begin + y * 2);
    }

    void CreateWall(int x, int y, int dx, int dy, int n, GameObject tile) {
        for (int i = 0; i < n; ++i) {
            CreateWallSegment(x + dx * i, y + dy * i, tile);
        }
    }

    void CreateLeftWall(GameObject tile) {
        CreateWall(0, 0, 0, 1, 15, tile);
    }

    void CreateLeftDoor(GameObject tile) {
        CreateWall(0, 0, 0, 1, 6, tile);
        CreateWall(0, 9, 0, 1, 6, tile);
    }

    void CreateRightWall(GameObject tile) {
        CreateWall(14, 0, 0, 1, 15, tile);
    }

    void CreateRightDoor(GameObject tile) {
        CreateWall(14, 0, 0, 1, 6, tile);
        CreateWall(14, 9, 0, 1, 6, tile);
    }

    void CreateUpWall(GameObject tile) {
        CreateWall(0, 14, 1, 0, 15, tile);
    }

    void CreateUpDoor(GameObject tile) {
        CreateWall(0, 14, 1, 0, 6, tile);
        CreateWall(9, 14, 1, 0, 6, tile);
    }

    void CreateDownWall(GameObject tile) {
        CreateWall(0, 0, 1, 0, 15, tile);
    }

    void CreateDownDoor(GameObject tile) {
        CreateWall(0, 0, 1, 0, 6, tile);
        CreateWall(9, 0, 1, 0, 6, tile);
    }

    GameObject CreateMapSegment(int x, int y) {
        GameObject map = Instantiate(map_segment);
        map.transform.position = new Vector3(30 * x, 0, 30 * y);
        return map;
    }

    GameObject CreateOpenMapSegment(int x, int y) {
        GameObject map = Instantiate(map_segment);
        map.transform.position = new Vector3(30 * x, 0, 30 * y);
        CreateUpDoor(map);
        CreateLeftDoor(map);
        CreateRightDoor(map);
        CreateDownDoor(map);
        return map;
    }

    int tile_num = 0;

    void AddEnemies(GameObject tile) {
        tile_num++;
        AddPickup(tile);
        int n = 1 + tile_num / enemy_scaling;
        for (int i = 0; i < n; ++i) {
            var enemy = Instantiate(enemy_group, tile.transform);
            var pos = enemy.transform.position;
            pos.x += UnityEngine.Random.Range(-10f, 10f);
            pos.z += UnityEngine.Random.Range(-10f, 10f);
            enemy.transform.position = pos;
        }
    }

    void AddPickup(GameObject tile) {
        if (weapon_pickups.Count > 0 && weapon_pickup_rate > UnityEngine.Random.Range(0f, 100f)) {
            int i = UnityEngine.Random.Range(0, weapon_pickups.Count) * UnityEngine.Random.Range(0, weapon_pickups.Count);
            if (weapon_pickups.Count > 0) {
                i = i / weapon_pickups.Count;
            }
            var p = weapon_pickups[i];
            weapon_pickups.RemoveAt(i);
            var enemy = Instantiate(p, tile.transform);
            var pos = enemy.transform.position;
            pos.x += UnityEngine.Random.Range(-2f, 2f);
            pos.z += UnityEngine.Random.Range(-2f, 2f);
            enemy.transform.position = pos;
        } else {
            var enemy = Instantiate(health_pickup, tile.transform);
            var pos = enemy.transform.position;
            pos.x += UnityEngine.Random.Range(-2f, 2f);
            pos.z += UnityEngine.Random.Range(-2f, 2f);
            enemy.transform.position = pos;
        }
    }

    void FillSegment(int x, int y) {
        var map = CreateOpenMapSegment(x, y);
        AddEnemies(map);
    }

    public Dictionary<Vector2Int, bool> open = new Dictionary<Vector2Int, bool>();

    public bool HasPos(int x, int y) {
        return open.ContainsKey(new Vector2Int(x, y));
    }

    public bool HasPos(Vector2Int x) {
        return HasPos(x.x, x.y);
    }

    int HasIntPos(int x, int y) {
        return HasPos(x, y) ? 1 : 0;
    }

    bool HasEnemy(int x, int y) {
        return open[new Vector2Int(x, y)];
    }

    void AddPos(int x, int y, bool has_enemy = true) {
        open[new Vector2Int(x, y)] = has_enemy;
    }

    int NeighbourCount(int x, int y) {
        return HasIntPos(x + 1, y) + HasIntPos(x - 1, y) + HasIntPos(x, y + 1) + HasIntPos(x, y - 1);
    }

    bool HasNeighbour(int x, int y) {
        return HasPos(x + 1, y) || HasPos(x - 1, y) || HasPos(x, y + 1) || HasPos(x, y - 1);
    }

    public float room_connect_chance = 5;

    bool MaybeAddPos(int x, int y) {
        if (HasPos(x, y)) {
            return false;
        }
        if (!HasNeighbour(x, y)) {
            return false;
        }
        if (NeighbourCount(x, y) > 2) {
            return false;
        }
        if (NeighbourCount(x, y) == 2) {
            if (UnityEngine.Random.Range(0f, 100f) > room_connect_chance) {
                return false;
            }
        }
        AddPos(x, y);
        return true;
    }

    GameObject CreateGeneratedSegment(int x, int y) {
        GameObject map = Instantiate(map_segment);
        map.transform.position = new Vector3(30 * x, 0, 30 * y);
        if (HasPos(x - 1, y)) {
            CreateLeftDoor(map);
        } else {
            CreateLeftWall(map);
        }
        if (HasPos(x + 1, y)) {
            CreateRightDoor(map);
        } else {
            CreateRightWall(map);
        }
        if (HasPos(x, y - 1)) {
            CreateDownDoor(map);
        } else {
            CreateDownWall(map);
        }
        if (HasPos(x, y + 1)) {
            CreateUpDoor(map);
        } else {
            CreateUpWall(map);
        }
        return map;
    }

    void CreateGeneratedTiles() {
        foreach (Vector2Int v in open.Keys) {
            GameObject map = CreateGeneratedSegment(v.x, v.y);
            if (HasEnemy(v.x, v.y)) {
                AddEnemies(map);
            }
        }
    }

    void CreateDemoMap() {
        CreateOpenMapSegment(1, 1);
        CreateOpenMapSegment(0, 1);
        CreateOpenMapSegment(-1, 1);
        CreateOpenMapSegment(1, 0);
        FillSegment(0, 0);
        CreateOpenMapSegment(-1, 0);
        CreateOpenMapSegment(1, -1);
        CreateOpenMapSegment(0, -1);
        CreateOpenMapSegment(-1, -1);
    }

    void CreateRandomMap(int n) {
        AddPos(0, 0, false);
        int ni = 1;
        while (ni < n) {
            int x = UnityEngine.Random.Range(-7, 8);
            int y = UnityEngine.Random.Range(-7, 8);
            if (MaybeAddPos(x, y)) {
                ni++;
            }
        }
    }

    // Start is called before the first frame update
    void Start() {
        CreateRandomMap(room_count);
        CreateGeneratedTiles();
    }

    // Update is called once per frame
    void Update() {

    }
}
