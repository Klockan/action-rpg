﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnDeath : MonoBehaviour {

    public GameObject spawn;

    bool is_quitting = false;

    void OnApplicationQuit() {
        is_quitting = true;
    }

    private void OnDestroy() {
        if (is_quitting) return;
        GameObject spawned = Instantiate(spawn);
        spawned.transform.position = transform.position;
    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
