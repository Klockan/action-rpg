﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnInit : MonoBehaviour {

    public GameObject spawn;

    // Start is called before the first frame update
    void Start() {
        GameObject spawned = Instantiate(spawn);
        spawned.transform.position = transform.position;
        spawned.transform.rotation = transform.rotation;
    }

    // Update is called once per frame
    void Update() {

    }
}
