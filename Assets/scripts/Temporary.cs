﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temporary : MonoBehaviour {

    public float life_time_sec = 1;

    float spawn_time;
    // Start is called before the first frame update
    void Start() {
        spawn_time = Time.time;
    }

    // Update is called once per frame
    void Update() {
        if (life_time_sec < Time.time - spawn_time) {
            Destroy(gameObject);
        }
    }
}
