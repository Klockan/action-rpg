﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {

    public float hp = 100;
    public float max_hp = 100;
    public float hp_regen = 10f;
    public float reactivate_threshold = 100f;
    public int faction = 10;

    bool disabled = false;

    public GameObject shield_effect;

    private void OnTriggerEnter(Collider collision) {
        if (disabled) return;
        if (hp < 0) {
            shield_effect.SetActive(false);
            disabled = true;
            return;
        }
        Bullet o = collision.gameObject.GetComponent<Bullet>();
        if (o == null) return;
        if (o.original_shooter.faction == faction) return;
        hp -= o.damage;
        Destroy(o.gameObject);
    }
    // Start is called before the first frame update
    void Start() {

        Killable k = GetComponentInParent<Killable>();
        if (k != null) {
            faction = k.faction;
        }

    }

    // Update is called once per frame
    void FixedUpdate() {
        hp += hp_regen / 50;
        if (hp > max_hp) {
            hp = max_hp;
        }
        if (hp >= reactivate_threshold && disabled) {
            disabled = false;
            shield_effect.SetActive(true);
        }
    }

    private void LateUpdate() {
        transform.rotation = Quaternion.identity;
    }
}
