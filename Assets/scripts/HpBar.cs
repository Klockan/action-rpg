﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBar : MonoBehaviour {

    public Killable body;

    public GameObject bar;

    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        float fraction = body.hp / body.max_hp;
        if (body.max_hp == 0) {
            fraction = 0.0f;
        }
        bar.transform.localPosition = new Vector3((fraction - 1.0f) / 2, 0, 0);
        bar.transform.localScale = new Vector3(fraction, 1, 1);
    }

    private void LateUpdate() {
        transform.rotation = Quaternion.identity;
    }
}
