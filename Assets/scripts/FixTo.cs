﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTo : MonoBehaviour {


    public GameObject target;

    Vector3 target_original_pos;

    Vector3 my_original_pos;

    // Start is called before the first frame update
    void Start() {
        target_original_pos = target.transform.position;
        my_original_pos = transform.position;
    }

    // Update is called once per frame
    void Update() {
        if (target != null) {
            Vector3 trans = target.transform.position - target_original_pos;
            transform.position = my_original_pos + trans;
        }
    }
}
