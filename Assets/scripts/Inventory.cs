﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {
    public List<Weapon> weapons;

    public UnityEngine.UI.Text ui_text;

    public void Add(Weapon weapon) {
        foreach (Weapon w in weapons) {
            if (weapon.weapon_name == w.weapon_name) {
                w.LevelUp();
                return;
            }
        }
        weapons.Add(Instantiate(weapon));
    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (ui_text != null) {
            string sep = "\n";
            string res = "";
            foreach (Weapon w in weapons) {
                res += w.AsString() + sep;
            }
            ui_text.text = res;
        }
    }
}
