﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public Bullet bullet;
    public Beam beam;
    public float fire_rate = 10;
    public float power_level = 10;
    public float speed_mod = 1;

    public int level = 1;

    public string weapon_name;

    public void LevelUp() {
        fire_rate = fire_rate * 1.2f;
        power_level = power_level * 1.2f;
        level++;
    }

    public string AsString() {
        if (level > 1) {
            return weapon_name + " (" + level + ")";
        }
        return weapon_name;
    }

    // Start is called before the first frame update
    void Start() {
        if (weapon_name == null) {
            throw new System.Exception();
        }
        if (bullet == null) bullet = GetComponent<Bullet>();
        if (beam == null) beam = GetComponent<Beam>();
    }

    // Update is called once per frame
    void Update() {

    }
}
