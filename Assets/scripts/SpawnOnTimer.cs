﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnTimer : MonoBehaviour {

    public GameObject spawn;
    public float seconds_between_spawns = 1;
    float next_spawn;

    // Start is called before the first frame update
    void Start() {
        next_spawn = Time.time;
    }

    // Update is called once per frame
    void Update() {
        if (next_spawn <= Time.time) {
            next_spawn = Time.time + seconds_between_spawns;
            GameObject spawned = Instantiate(spawn);
            spawned.transform.position = transform.position;
            spawned.transform.rotation = transform.rotation;
        }

    }
}
