﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beam : MonoBehaviour {

    public float damage = 1;
    public float tick_intervall = 0.1f;
    public float length = 10.0f;
    public int faction = 10;
    float next_tick;
    public Vector3 dir = new Vector3(0, 1, 0);

    public Killable original_shooter;

    // Start is called before the first frame update
    void Start() {
        next_tick = Time.time;
    }

    void ShootRay() {
        RaycastHit hit;
        var transformed_dir = transform.TransformDirection(dir);
        var my_pos = transform.position;
        Ray ray = new Ray(my_pos, transformed_dir);
        if (Physics.Raycast(ray, out hit, length)) {
            GameObject o = hit.collider.gameObject;
            Killable k = o.GetComponentInParent<Killable>();
            if (k != null) {
                if (k.faction == faction) Debug.Log("Hit yourself!");
                k.hp -= damage;
                if (k.hp + damage > 0 && k.hp <= 0) {
                    if (original_shooter != null) {
                        original_shooter.kill_count += 1;
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (next_tick <= Time.time) {
            next_tick = Time.time + tick_intervall;
            ShootRay();
        }
    }
}
