﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static GameObject player;

    Rigidbody me;

    public float speed = 10.0f;

    Shooter gun;

    WeaponList weapons;


    // Start is called before the first frame update
    void Start() {
        weapons = GetComponent<WeaponList>();
        player = gameObject;
        me = GetComponent<Rigidbody>();
        gun = GetComponent<Shooter>();
    }

    private void RotationHandler() {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f)) {
            gun.LookAt(hit.point);
        }
    }

    private void ClickHandler() {
        if (Input.GetMouseButton(0) && gun.CanFire()) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f)) {
                gun.ShootAt(hit.point, me.velocity);
            }
        }
    }

    private void ChangeWeaponHandler() {
        if (Input.GetKeyDown("1")) {
            if (weapons.standard != null) {
                gun.bullet = weapons.standard;
                gun.beam = null;
            }
        }
        if (Input.GetKeyDown("2")) {
            if (weapons.fireball != null) {
                gun.bullet = weapons.fireball;
                gun.beam = null;
            }
        }
        if (Input.GetKeyDown("3")) {
            if (weapons.lightning_bolt != null) {
                gun.bullet = weapons.lightning_bolt;
                gun.beam = null;
            }
        }
        if (Input.GetKeyDown("4")) {
            if (weapons.lightning_beam != null) {
                gun.beam = weapons.lightning_beam;
                gun.bullet = null;
            }
        }
    }

    private void Move() {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        me.velocity = new Vector3(x * speed, me.velocity.y, y * speed);

    }

    // Update is called once per frame
    void Update() {
        Move();
        RotationHandler();
        ClickHandler();
        ChangeWeaponHandler();
    }
}
