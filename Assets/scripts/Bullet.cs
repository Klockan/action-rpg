﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float velocity = 20.0f;


    public Vector3 dir = new Vector3(0, 1, 0);

    public float damage = 10;

    public int faction = 1;

    public Killable original_shooter;

    // Start is called before the first frame update
    void Start() {
        GetComponent<Rigidbody>().velocity = dir * velocity;
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.GetComponent<BulletRegisterer>() != null) return;
        Killable o = collision.gameObject.GetComponent<Killable>();
        if (o != null) {
            o.hp -= damage;
            if (o.hp + damage > 0 && o.hp <= 0) {
                if (original_shooter != null) {
                    original_shooter.kill_count += 1;
                }
            }
        }
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update() {
        GetComponent<Rigidbody>().velocity = dir * velocity;
    }
}
