﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public static List<Pickup> pickups = new List<Pickup>();

    public static void MarkCleared(Vector2Int pos) {
        foreach (Pickup p in pickups) {
            if (p != null && pos == p.map_pos) {
                p.guarded = false;
            }
        }
    }

    public static Pickup ClosestUnguardedHealthPickup(Vector3 pos) {
        float d = 1E9f;
        Pickup res = null;
        foreach (Pickup p in pickups) {
            if (p == null || p.guarded) continue;
            if (p.hp > 0 && (p.transform.position - pos).magnitude < d) {
                d = (p.transform.position - pos).magnitude;
                res = p;
            }
        }
        return res;
    }

    public static Pickup ClosestWeaponPickup(Vector3 pos) {
        float d = 1E9f;
        Pickup res = null;
        foreach (Pickup p in pickups) {
            if (p == null) continue;
            if (p.IsWeaponPickup() && (p.transform.position - pos).magnitude < d) {
                d = (p.transform.position - pos).magnitude;
                res = p;
            }
        }
        return res;
    }

    public float hp = 5;
    public Weapon weapon;
    public bool guarded = true;
    public Vector2Int map_pos;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject == null) return;
        if (other.gameObject.GetComponent<BulletRegisterer>() != null) return;
        Killable o = other.gameObject.GetComponent<Killable>();
        if (o == null) return;
        if (o.faction != 10) return;
        if (o.hp < o.max_hp && hp > 0) {
            o.hp += hp;
            if (o.hp > o.max_hp) o.hp = o.max_hp;
            Destroy(gameObject);
        }
        if (weapon != null) {
            Inventory inventory = other.gameObject.GetComponentInParent<Inventory>();
            if (inventory != null) {
                inventory.Add(weapon);
                Destroy(gameObject);
            }
        }
    }

    bool IsWeaponPickup() {
        return weapon != null;
    }

    // Start is called before the first frame update
    void Start() {
        pickups.Add(this);
        map_pos = WorldGen.ToMapPos(transform.position);
    }

    // Update is called once per frame
    void Update() {

    }
}
