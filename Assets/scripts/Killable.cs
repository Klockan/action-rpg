﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killable : MonoBehaviour {

    public static int FRIEND = 10;

    public static List<Killable> entities = new List<Killable>();

    public GameObject icon;

    public static GameObject getNearestEnemy(int faction, Vector3 my_pos) {
        float dist = 1E12f;
        GameObject enemy = null;
        foreach (Killable o in Killable.entities) {
            if (o != null && o.faction != faction && dist > (my_pos - o.transform.position).sqrMagnitude) {
                enemy = o.gameObject;
                dist = (my_pos - o.transform.position).sqrMagnitude;
            }
        }
        return enemy;
    }

    public int faction = 10;
    public int kill_count = 0;
    public float max_hp = 100;
    public float hp = 100;
    // Start is called before the first frame update
    void Start() {
        entities.Add(this);
        hp = max_hp;
    }

    private void OnDestroy() {
        if (icon != null) {
            Destroy(icon);
        }
    }

    // Update is called once per frame
    void Update() {
        if (hp <= 0) {
            Destroy(gameObject);
        }
    }
}
