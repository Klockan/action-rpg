﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponList : MonoBehaviour {
    public Bullet standard;
    public Bullet fireball;
    public Bullet lightning_bolt;
    public Beam lightning_beam;

    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {

    }
}
