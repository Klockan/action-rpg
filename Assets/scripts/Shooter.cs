﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

    public Bullet bullet;
    public Beam beam;
    public GameObject bullet_spawn_pos;
    public float fire_rate = 1;

    public float speed_mod = 1;

    float next_fire;

    public float TimeToDistance(float distance) {
        return distance / speed_mod / bullet.velocity;
    }


    Killable me;

    public void Equip(Weapon weapon) {
        bullet = weapon.bullet;
        beam = weapon.beam;
        fire_rate = weapon.fire_rate;
        speed_mod = weapon.speed_mod;
    }

    public bool CanFire() {
        return next_fire <= Time.time;
    }

    public static float FlatAngle(Vector3 a, Vector3 b) {
        return -Vector2.SignedAngle(new Vector2(a.x, a.z), new Vector2(b.x, b.z));
    }

    public void ShootAt(Vector3 target, Vector3 shooter_speed) {
        if (!CanFire()) return;
        next_fire = Time.time + 1.0f / fire_rate;
        var dir = target - transform.position;
        dir.y = 0;
        if (dir.magnitude == 0) {
            dir = new Vector3(1E9f, 0f, 1E9f);
        }
        if (bullet != null) {
            Bullet new_bullet = Instantiate(bullet);
            new_bullet.transform.position = bullet_spawn_pos.transform.position;
            dir.y = 0;
            new_bullet.dir = dir.normalized;
            Vector3 bullet_velocity = new_bullet.dir * new_bullet.velocity;
            bullet_velocity += shooter_speed;
            new_bullet.velocity = bullet_velocity.magnitude * speed_mod;
            new_bullet.dir = bullet_velocity.normalized;
            float angle = FlatAngle(Vector3.forward, new_bullet.dir);
            new_bullet.transform.rotation = Quaternion.AngleAxis(angle, Vector2.up);
            new_bullet.original_shooter = me;
        } else {
            Beam new_beam = Instantiate(beam, bullet_spawn_pos.transform);
            new_beam.original_shooter = me;
        }
    }

    public void LookAt(Vector3 target, float deviation=0) {
        var dir = target - transform.position;
        dir.y = 0;
        if (dir.magnitude == 0) {
            dir = new Vector3(1E9f, 0f, 1E9f);
        }
        float angle = FlatAngle(Vector3.forward, dir);
        transform.rotation = Quaternion.AngleAxis(angle + deviation, Vector2.up);
    }

    public Vector3 TargetDir(Vector3 target) {
        var dir = target - transform.position;
        dir.y = 0;
        if (dir.magnitude == 0) {
            dir = new Vector3(1E9f, 0f, 1E9f);
        }
        return dir.normalized;
    }

    // Start is called before the first frame update
    void Start() {
        me = GetComponent<Killable>();
    }

    // Update is called once per frame
    void Update() {

    }
}
